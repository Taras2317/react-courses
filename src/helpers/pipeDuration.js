export function transformDuration(number) {
	const hours = Math.floor(number / 60);
	let minutes = number % 60;
	minutes = minutes < 10 ? '0' + minutes : minutes;
	return `${hours}:${minutes}`;
}
