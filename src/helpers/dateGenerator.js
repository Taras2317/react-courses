export const dateGenerator = (date) => {
	return new Date(date).toLocaleDateString('en-GB').replace(/\//g, '.');
};
