import React, { useState } from 'react';

import Input from '../../../common/Input/Input';
import { transformDuration } from '../../../helpers/pipeDuration';
import { DUR_LBL, DUR_PLC_HLD } from '../../../constants';

import classes from './Duration.module.css';
import '../global.css';

function Duration(props) {
	const [hours, setHours] = useState();

	function changeHandler(event) {
		setHours(transformDuration(event.target.value));
		props.onDurationSet(+event.target.value);
	}

	return (
		<div className='authors'>
			<p className='heading2'>Duration</p>
			<Input
				id='duration'
				type='number'
				labelText={DUR_LBL}
				placeholderText={DUR_PLC_HLD}
				onChange={changeHandler}
				required={true}
				min={1}
			/>
			<div className={classes.result}>
				<h2>
					Duration: <span>{!hours ? '00:00' : hours}</span> hours
				</h2>
			</div>
		</div>
	);
}

export default Duration;
