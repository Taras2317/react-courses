import React, { useContext } from 'react';

import Button from '../../../common/Button/Button';
import { BTN_ADD_AUTH } from '../../../constants';
import AuthorsContext from '../../../store/authors-context';

import classes from './Author.module.css';

function Author(props) {
	const context = useContext(AuthorsContext);

	const addHandler = () => context.addAuthor(props.author);
	const deleteHandler = () => context.removeAuthor(props.author);

	return (
		<div className={classes.subrow}>
			<span>{props.authorName}</span>
			<Button
				buttonText={props.btnText}
				onClick={props.btnText === BTN_ADD_AUTH ? addHandler : deleteHandler}
			/>
		</div>
	);
}

export default Author;
