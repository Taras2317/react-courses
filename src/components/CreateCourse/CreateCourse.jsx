import React, { useState } from 'react';
import { v4 as uuidv4 } from 'uuid';

import Description from './Description/Description';
import Authors from './Authors/Authors';
import AddAuthor from './AddAuthor/AddAuthor';
import Duration from './Duration/Duration';
import CourseAuthors from './CourseAuthors/CourseAuthors';
import Input from '../../common/Input/Input';
import Button from '../../common/Button/Button';
import Card from '../../common/Card/Card';
import {
	COURSE_ALERT,
	TITLE_LBL,
	TITLE_PLC_HLD,
	BTN_CREATE_COURSE,
} from '../../constants';
import AuthorsContext from '../../store/authors-context';

import classes from './CreateCourse.module.css';

function CreateCourse(props) {
	const [authors, setAuthors] = useState(props.authors);
	const [courseAuthors, setCourseAuthors] = useState([]);

	const [title, setTitle] = useState('');
	const [description, setDescription] = useState('');
	const [duration, setDuration] = useState('');

	const getTitle = (e) => setTitle(e.target.value);
	const getDescription = (data) => setDescription(data);
	const getDuration = (data) => setDuration(data);

	function addAuthor(author) {
		setAuthors((prevAuthors) => {
			return [...prevAuthors, author];
		});
		props.onAddAuthor(author);
	}

	function addAuthorToCourseAuthors(author) {
		setCourseAuthors((prevAuthors) => {
			return [...prevAuthors, author];
		});
		setAuthors((prevAuthors) => {
			const updatedAuthors = prevAuthors.filter(
				(authoR) => authoR.id !== author.id
			);
			return updatedAuthors;
		});
	}

	function removeAuthorFromCourseAuthors(author) {
		setAuthors((prevAuthors) => {
			return [...prevAuthors, author];
		});
		setCourseAuthors((prevAuthors) => {
			const updatedAuthors = prevAuthors.filter(
				(authoR) => authoR.id !== author.id
			);
			return updatedAuthors;
		});
	}

	function emit(e) {
		e.preventDefault();
		if (courseAuthors.length === 0) {
			alert(COURSE_ALERT);
			return;
		}

		props.onCreateCourse({
			id: uuidv4(),
			title: title,
			description: description,
			creationDate: new Date(),
			duration: duration,
			authors: courseAuthors.map((el) => el.id),
		});
		props.onCreateClick();
	}

	return (
		<form onSubmit={emit}>
			<Card className={classes.create_course}>
				<div className={classes.create_course_top}>
					<div className={classes.title}>
						<Input
							id='title'
							labelText={TITLE_LBL}
							placeholderText={TITLE_PLC_HLD}
							onChange={getTitle}
							required={true}
						/>
					</div>
					<div className={classes.btn_create}>
						<Button buttonText={BTN_CREATE_COURSE} type='submit' />
					</div>
				</div>
				<Description onDescriptionSet={getDescription} />
				<div className={classes.authors}>
					<AuthorsContext.Provider
						value={{
							addAuthor: addAuthorToCourseAuthors,
							removeAuthor: removeAuthorFromCourseAuthors,
						}}
					>
						<div className={classes.authors_row1}>
							<AddAuthor onAuthorCreate={addAuthor} />
							<Authors authors={authors} />
						</div>
						<div className={classes.authors_row2}>
							<Duration onDurationSet={getDuration} />
							<CourseAuthors authors={courseAuthors} />
						</div>
					</AuthorsContext.Provider>
				</div>
			</Card>
		</form>
	);
}

export default CreateCourse;
