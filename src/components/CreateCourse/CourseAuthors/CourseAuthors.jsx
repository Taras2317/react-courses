import React from 'react';

import Author from '../Author/Author';
import { BTN_DELETE_AUTH } from '../../../constants';

import '../global.css';

function CourseAuthors(props) {
	function deleteAuthor(author) {
		props.onDeleteAuthor(author);
	}

	return (
		<div className='authors'>
			<p className='heading'>Course Authors</p>
			{props.authors.map((author) => (
				<Author
					author={author}
					authorName={author.name}
					btnText={BTN_DELETE_AUTH}
					key={author.id}
					onDelete={deleteAuthor}
				/>
			))}
		</div>
	);
}

export default CourseAuthors;
