import React, { useState } from 'react';
import { v4 as uuidv4 } from 'uuid';

import Input from '../../../common/Input/Input';
import Button from '../../../common/Button/Button';
import {
	ADD_AUTH_LBL,
	ADD_AUTH_PLC_HLD,
	BTN_CREATE_AUTH,
} from '../../../constants';

import '../global.css';

function AddAuthor(props) {
	const [authorName, setAuthorName] = useState('');

	const authorNameInputHandler = (e) => setAuthorName(e.target.value);

	function createAuthor() {
		props.onAuthorCreate({ id: uuidv4(), name: authorName });
		setAuthorName('');
	}

	return (
		<div className='authors'>
			<p className='heading2'>Add Author</p>
			<Input
				id='addAuthor'
				labelText={ADD_AUTH_LBL}
				placeholderText={ADD_AUTH_PLC_HLD}
				onChange={authorNameInputHandler}
				value={authorName}
			/>
			<div style={{ marginTop: '1rem' }}>
				<Button
					buttonText={BTN_CREATE_AUTH}
					onClick={createAuthor}
					disabled={authorName.length < 2 ? true : false}
				/>
			</div>
		</div>
	);
}

export default AddAuthor;
