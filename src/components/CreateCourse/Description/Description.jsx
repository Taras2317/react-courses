import React from 'react';

import { DESC_LBL, DESC_PLC_HLD, DESC_TXTAREA_SIZE } from '../../../constants';

import classes from './Description.module.css';

function Description(props) {
	const setDescrition = (e) => props.onDescriptionSet(e.target.value);

	return (
		<div className={classes.description}>
			<label htmlFor={DESC_LBL}>{DESC_LBL}</label>
			<textarea
				id={DESC_LBL}
				cols={DESC_TXTAREA_SIZE.col}
				rows={DESC_TXTAREA_SIZE.row}
				placeholder={DESC_PLC_HLD}
				minLength={2}
				required={true}
				onChange={setDescrition}
			></textarea>
		</div>
	);
}

export default Description;
