import React from 'react';

import Author from '../Author/Author';
import { BTN_ADD_AUTH } from '../../../constants';

import '../global.css';

function Authors(props) {
	return (
		<div className='authors'>
			<p className='heading'>Authors</p>
			{props.authors.map((author) => (
				<Author
					author={author}
					authorName={author.name}
					btnText={BTN_ADD_AUTH}
					key={author.id}
				/>
			))}
		</div>
	);
}

export default Authors;
