import React, { useState } from 'react';

import Button from '../../../../common/Button/Button';
import Input from '../../../../common/Input/Input';
import { BTN_SEARCH } from '../../../../constants';
import { SEARCH_PLC_HLD } from '../../../../constants';

import classes from './SearchBar.module.css';

function SearchBar(props) {
	const [inputValue, setinputValue] = useState();

	function inputChangeHandler(e) {
		if (e.target.value.trim() === '') {
			props.onSearch('');
			return;
		}
		setinputValue(e.target.value);
	}
	const searchCourse = () => props.onSearch(inputValue);

	return (
		<div className={classes.search}>
			<div className={classes.mr}>
				<Input placeholderText={SEARCH_PLC_HLD} onChange={inputChangeHandler} />
			</div>
			<Button buttonText={BTN_SEARCH} onClick={searchCourse} />
		</div>
	);
}

export default SearchBar;
