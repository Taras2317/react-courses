import React from 'react';

import Button from '../../../../common/Button/Button';
import { transformDuration } from '../../../../helpers/pipeDuration';
import { dateGenerator } from '../../../../helpers/dateGenerator';
import { BTN_SHOW } from '../../../../constants';

import classes from './CourseCard.module.css';

function CourseCard(props) {
	const courseAuthors = props.authors
		.map((id) => props.authorsObj[id])
		.join(', ');

	const courseDuration = transformDuration(props.duration) + ' hours';

	return (
		<li className={classes.courseCard} id={props.id}>
			<div className={classes.cardLeft}>
				<h2>{props.title}</h2>
				<p>{props.description}</p>
			</div>
			<div className={classes.cardRight}>
				<div>
					<p>
						<span>Authors: </span> {courseAuthors}
					</p>
					<p>
						<span>Duration: </span> {courseDuration}
					</p>
					<p>
						<span>Created: </span> {dateGenerator(props.date)}
					</p>
				</div>
				<div className={classes.centered}>
					<Button buttonText={BTN_SHOW}></Button>
				</div>
			</div>
		</li>
	);
}

export default CourseCard;
