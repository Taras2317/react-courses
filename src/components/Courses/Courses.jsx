import React, { useState } from 'react';

import Card from '../../common/Card/Card';
import Button from '../../common/Button/Button';
import SearchBar from './components/SearchBar/SearchBar';
import CourseCard from './components/CourseCard/CourseCard';
import { BTN_ADD_COURSE } from '../../constants';

import classes from './Courses.module.css';

function Courses(props) {
	const [courses, setCourses] = useState(props.courses);

	const authorsObj = {};
	props.authors.forEach((author) => {
		const { id, name } = author;
		authorsObj[id] = name;
	});

	function filterCourses(searchQuery) {
		const filtered = props.courses.filter((course) => {
			return (
				course.title.toLowerCase().includes(searchQuery.toLowerCase()) ||
				course.id.toLowerCase() === searchQuery.toLowerCase()
			);
		});
		setCourses(filtered);
	}

	return (
		<Card className={classes.courses}>
			<div className={classes.bar}>
				<SearchBar onSearch={filterCourses} />
				<Button buttonText={BTN_ADD_COURSE} onClick={props.onCreateClick} />
			</div>
			<ul>
				{courses.map((course) => (
					<CourseCard
						key={course.id}
						id={course.id}
						title={course.title}
						description={course.description}
						authors={course.authors}
						duration={course.duration}
						date={course.creationDate}
						authorsObj={authorsObj}
					/>
				))}
			</ul>
		</Card>
	);
}

export default Courses;
