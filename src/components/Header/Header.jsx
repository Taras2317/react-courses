import React from 'react';

// import Button from '../../common/Button/Button';
import Card from '../../common/Card/Card';
import Logo from './components/Logo/Logo';
// import { BTN_LOGOUT } from '../../constants';

import classes from './Header.module.css';

function Header() {
	return (
		<Card className={classes.header}>
			<Logo />
			{/* <div className={classes.user}>
				<p>Taras</p>
				<Button buttonText={BTN_LOGOUT}></Button>
			</div> */}
		</Card>
	);
}

export default Header;
