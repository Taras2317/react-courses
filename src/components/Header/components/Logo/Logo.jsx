import React, { useContext } from 'react';

import logo from '../../../../assets/logo.png';
import HeaderContext from '../../../../store/header-context';

import classes from './Logo.module.css';

function Logo() {
	const context = useContext(HeaderContext);

	return (
		<img
			alt='logo'
			src={logo}
			className={classes.logo}
			onClick={context.goHome}
		></img>
	);
}

export default Logo;
