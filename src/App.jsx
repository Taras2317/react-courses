import React, { useState } from 'react';

import Header from './components/Header/Header';
import Courses from './components/Courses/Courses';
import CreateCourse from './components/CreateCourse/CreateCourse';
import { mockedCoursesList, mockedAuthorsList } from './constants';
import HeaderContext from './store/header-context';

function App() {
	const [courses, setCourses] = useState(mockedCoursesList);
	const [authors, setAuthors] = useState(mockedAuthorsList);
	const [isOpened, setIsOpened] = useState(false);

	const toggleCreateCourse = () => setIsOpened(!isOpened);
	const goHome = () => setIsOpened(false);

	function createCourse(course) {
		setCourses((prevState) => {
			return [...prevState, course];
		});
	}
	function createAuthor(author) {
		setAuthors((prevState) => {
			return [...prevState, author];
		});
	}

	return (
		<React.Fragment>
			<HeaderContext.Provider value={{ goHome: goHome }}>
				<Header />
			</HeaderContext.Provider>
			{!isOpened && (
				<Courses
					onCreateClick={toggleCreateCourse}
					courses={courses}
					authors={authors}
				/>
			)}
			{isOpened && (
				<CreateCourse
					onCreateClick={toggleCreateCourse}
					onCreateCourse={createCourse}
					authors={authors}
					onAddAuthor={createAuthor}
				/>
			)}
		</React.Fragment>
	);
}

export default App;
