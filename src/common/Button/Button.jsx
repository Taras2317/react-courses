import React from 'react';

import classes from './Button.module.css';

function Button(props) {
	return (
		<button
			className={classes.button}
			type={props.type || 'button'}
			onClick={props.onClick}
			disabled={props.disabled}
		>
			{props.buttonText}
		</button>
	);
}

export default Button;
