import React from 'react';

import classes from './Input.module.css';

function Input(props) {
	return (
		<div className={classes.input}>
			<label htmlFor={props.id}>{props.labelText}</label>
			<input
				type={props.type || 'text'}
				id={props.id}
				value={props.value}
				onChange={props.onChange}
				placeholder={props.placeholderText}
				minLength={props.minLength}
				min={props.min}
				required={props.required}
			/>
		</div>
	);
}

export default Input;
