export const mockedCoursesList = [
	{
		id: 'de5aaa59-90f5-4dbc-b8a9-aaf205c551ba',
		title: 'JavaScript',
		description: `Lorem Ipsum is simply dummy text of the printing and
  typesetting industry. Lorem Ipsum
  has been the industry's standard dummy text ever since the
  1500s, when an unknown
  printer took a galley of type and scrambled it to make a type
  specimen book. It has survived
  not only five centuries, but also the leap into electronic typesetting, remaining essentially u
  nchanged.`,
		creationDate: '8/3/2021',
		duration: 160,
		authors: [
			'27cc3006-e93a-4748-8ca8-73d06aa93b6d',
			'f762978b-61eb-4096-812b-ebde22838167',
		],
	},
	{
		id: 'b5630fdd-7bf7-4d39-b75a-2b5906fd0916',
		title: 'Angular',
		description: `Lorem Ipsum is simply dummy text of the printing and
		typesetting industry. Lorem Ipsum
		has been the industry's standard dummy text ever since the
		1500s, when an unknown
		printer took a galley of type and scrambled it to make a type
		specimen book. It has survived
		not only five centuries, but also the leap into electronic typesetting, remaining essentially u
		nchanged.`,
		creationDate: '10/11/2020',
		duration: 210,
		authors: [
			'df32994e-b23d-497c-9e4d-84e4dc02882f',
			'095a1817-d45b-4ed7-9cf7-b2417bcbf748',
		],
	},
];

export const mockedAuthorsList = [
	{
		id: '27cc3006-e93a-4748-8ca8-73d06aa93b6d',
		name: 'Sundar Pichai',
	},
	{
		id: 'f762978b-61eb-4096-812b-ebde22838167',
		name: 'Bill Gates',
	},
	{
		id: 'df32994e-b23d-497c-9e4d-84e4dc02882f',
		name: 'Hideo Kojima',
	},
	{
		id: '095a1817-d45b-4ed7-9cf7-b2417bcbf748',
		name: 'Steve Jobs',
	},
];

export const BTN_LOGOUT = 'Logout';
export const BTN_SEARCH = 'Search';
export const BTN_ADD_COURSE = 'Add new course';
export const BTN_SHOW = 'Show course';
export const BTN_CREATE_COURSE = 'Create course';
export const BTN_CREATE_AUTH = 'Create author';
export const BTN_ADD_AUTH = 'Add author';
export const BTN_DELETE_AUTH = 'Delete author';

export const SEARCH_PLC_HLD = 'Enter course name...';

export const TITLE_LBL = 'Title';
export const TITLE_PLC_HLD = 'Enter title...';

export const DESC_LBL = 'Description';
export const DESC_PLC_HLD = 'Enter description...';
export const DESC_TXTAREA_SIZE = { col: '30', row: '6' };

export const ADD_AUTH_LBL = 'Author name';
export const ADD_AUTH_PLC_HLD = 'Enter author name...';

export const DUR_LBL = 'Duration';
export const DUR_PLC_HLD = 'Enter duration in minutes...';

export const COURSE_ALERT =
	'Please, fill in all the info (title, description, duration, add course authors)';
